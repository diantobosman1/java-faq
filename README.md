# faq

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A respoitory for collecting and answering questions

## Table of Contents

- [Asking Questions](#asking-questions)
- [Contributing](#contributing)
- [License](#license)

## Asking Questions

If you have a question from our course then you can create an issue here to have your question answered. When submitting questions, please ensure the following:

1. Please search the existing questions to see if there are any similar questions.
2. If you are unsure if your issue is the same as another then you can create a new issue and reference the ones you suspect are similar.
3. Make sure you follow the issue template and supply as much detail as possible.
4. Once your question is answered, please specify precisely what resolved your problem so that others can also benefit from your question.

## Contributing

There are many ways to contribute to this repository:

1. **Asking questions!** It helps you, it helps your peers and future course participants, and it helps us improve the course and identify areas that require clarification.
2. **Answering questions!** The only thing more beneficial to learning than asking questions is answering them. Please feel free to directly participate in threads that are not of your own creation.
3. **Marking outdated questions!** Inevitably as the course and technologies develop some of the questions you see might no longer be applicable. Help us identify these so we can remove them from the FAQ.
4. **Suggesting improvments!** If you have an idea that would improve the process for asking and answering questions the we would love to know about it.
5. **Supporting suggestions!** If you like a suggestion that someone else has made then show your support with a 👍 or 🚀.
6. **Squashing bugs!** Found a bug? Or a typo? We'd appreciate a merge request that corrects the mistake.
7. **Build something!** Going one step past suggesting an improvement; you may wish to build the improvement yourself. We'd love to incorportate your contribution but please make sure to open an issue to discuss the idea first to prevent any wasted effort.

PRs accepted for new features (after discussion) and for corrections and fixes.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2021 Noroff Accelerate AS
