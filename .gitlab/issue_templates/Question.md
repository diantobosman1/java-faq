## Summary

<!-- Summarize the question concisely -->

## Is this related to a particular module or assignment? Which one?

<!-- Please delete if not applicable -->

## What have you read/watched so far?

<!-- If applicable, please tell us what resources you have consulted so far, including possible fixes, and/or similar issues -->
<!-- Please note: if your question is related to a project of your own and you require assistance debugging then this question is mandatory -->

## What is your current understanding?

<!-- If applicable, please tell us what you think _should_ be happening or how you think something works -->
<!-- Please note: if your question is related to a project of your own and you require assistance debugging then this question is mandatory -->

## Example project, logs and screenshots

<!-- If applicable, please create an example project here on GitLab that exhibits the problematic behavior, and link to it here in the bug report. Alternatively please provide relevant logs and screenshots. -->
<!-- Please note: if your question is related to a project of your own and you require assistance debugging then this question is mandatory -->

<!-- Please do not remove the line below! -->

/label ~action::triage ~faq::question
